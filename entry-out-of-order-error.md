Loki doesn't accept logs older than logs already received for a given stream (stream = tenant + labels).

Normally, everything works just fine. However, as soon as a distributor became unavailable, we noticed that an error "entry out of order" appeared (the output was formatted to be more readable) :

![alt text](images/entry-out-of-order-logs.png "Entry out of order logs")

This error have for consequence to display in Grafana different results for the same read request. Here, 2 succesive requests for an application from our project where we can see a log loss :

![alt text](images/entry-out-of-order-explore-full-logs.png "Full logs in Explore")

![alt text](images/entry-out-of-order-explore-missing-logs.png "Missing logs in Explore")

This loss of logs is temporary : once chunks are transfered on the bucket, logs were complete again.
This error was appearing during RollingUpdates too.

In this example, we had 3 ingesters with a `replication_factor` of 2. In the end, because logs are stored in an s3 bucket, we decided to stick with a `replication_factor` of 1.
