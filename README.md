# Prod-ready Loki on Kubernetes 

This repository contains example manifests to deploy a Prod ready architecture on Kubernetes.
The Loki manifests doesn't contain any templating to be used as you want (with Helm, Ansible ...).
This example is based on AWS cloud provider and uses S3 and Elasticache Redis services.

## Prerequisites

As we use AWS services, we need credentials with the right permissions.
You can find the permission needed on S3 in [Loki documentation](https://grafana.com/docs/loki/latest/operations/storage/).

Once a user with those permissions is created, you can update `loki/secret.yaml` with its credentials.

Depending on your caching solution, you can update the `loki/secret.yaml` to use Redis or not.

## Multi-tenancy

Just add a Grafana Loki datasource and use [custom http headers](https://grafana.com/docs/grafana/latest/administration/provisioning/#custom-http-headers-for-datasources) to access tenants.

## Deployment

```bash
$ kubectl apply -R -f loki/
```
